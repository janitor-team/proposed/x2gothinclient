# Tools for using X2Go in a thin client environment.
# Copyright (C) 2005-2018 The X2Go Project
# This file is distributed under the same license as the x2gothinclient package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: x2gothinclient 1.5.0.0\n"
"Report-Msgid-Bugs-To: submit@bugs.x2go.org\n"
"POT-Creation-Date: 2013-03-29 16:53+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: x2go-i18n mailing list <x2go-i18n@lists.x2go.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../x2gothinclient-displaymanager.templates:1001
msgid "Default display manager:"
msgstr ""

#. Type: select
#. Description
#: ../x2gothinclient-displaymanager.templates:1001
msgid ""
"On X2Go thin clients X2Go Client is sort of used as a display manager. For "
"this, X2Go Client gets started in TCE mode. The TCE acronym stands for thin "
"client environment. In TCE mode, X2Go Client manages the default display of "
"the X Window System."
msgstr ""

#. Type: select
#. Description
#: ../x2gothinclient-displaymanager.templates:1001
msgid ""
"Generally, a display manager is a program that provides graphical login "
"capabilities for the X Window System. Other display managers for example are "
"GDM, KDM, etc. Login is--in most cases--granted to the local system."
msgstr ""

#. Type: select
#. Description
#: ../x2gothinclient-displaymanager.templates:1001
msgid ""
"However, X2Go Client in TCE mode does appear like a display manager, but it "
"will log you onto pre-defined X2Go sessions on remote servers."
msgstr ""

#. Type: select
#. Description
#: ../x2gothinclient-displaymanager.templates:1001
msgid ""
"As you are about to install X2Go Client in TCE mode on this machine and as "
"you already have other display managers installed on this machine, please "
"explicitly select which display manager is supposed to be the default for "
"your system."
msgstr ""
